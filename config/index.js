// config/index.js
'use strict'
const path = require('path')
console.log(path.resolve(__dirname, '../public/assets/build'));
module.exports = {
  dev: {
    // Paths
    assetsSubDirectory: 'assets',
    assetsPublicPath: '/',
    proxyTable: {},

    // Various Dev Server settings
    host: 'localhost',
    port: 8000, 

    // skipping other options as they are only convenience features
  },
  build: {
    // Template for index.html
    index: path.resolve(__dirname, '../fuel/app/views/main.php'),

    // Paths
    assetsRoot: path.resolve(__dirname, '../public'),
    assetsSubDirectory: 'assets',
    assetsPublicPath: '/',

    productionSourceMap: true,

    // skipping the rest ...
  },
}